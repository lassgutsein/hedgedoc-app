#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const LOCATION = 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    const username = process.env.USERNAME, password = process.env.PASSWORD;
    let noteUrl;

    async function login(username) {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//button[text()="Sign In"]')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//div[@class="ui-signin"]/button[text()="Sign In"]')).click();
        await browser.sleep(2000); // wait for login popup
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Sign In" and contains(@formaction, "ldap")]')).click();
        await browser.sleep(5000);
    }

    async function newNote() {
        await browser.get('https://' + app.fqdn + '/new');
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Publish")]')), TEST_TIMEOUT);
        await browser.sleep(8000); // code mirror takes a while to load
        noteUrl = await browser.getCurrentUrl();
        console.log('The note url is ' + noteUrl);
        await browser.sleep(5000);
        await browser.findElement(By.css('.CodeMirror textarea')).sendKeys('hello cloudron');
        await browser.sleep(4000); // give it a second to 'save'
    }

    async function checkExistingNote() {
        await browser.get(noteUrl);
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "hello cloudron")]')), TEST_TIMEOUT);
    }

    async function checkNoteIsEditablePermission() {
        await browser.get(noteUrl);
        await browser.sleep(3000);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//p[contains(text(), "hello cloudron")]'))), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//a[@id="permissionLabel" and @title="Signed people can edit"]'))), TEST_TIMEOUT);
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//button[@id="profileLabel"]'))), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//button[@id="profileLabel"]')).click();
        await browser.sleep(5000); // wait for menu to open
        await browser.findElement(By.xpath('//a[contains(text(), "Sign Out")]')).click();
        await browser.sleep(5000);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];

        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username));
    it('can create new note', newNote);
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });


    it('can login', login.bind(null, username));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can login', login.bind(null, username));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('move to different location', function (done) {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
        noteUrl = noteUrl.replace(LOCATION, LOCATION + '2');

        done();
    });

    it('can login', login.bind(null, username));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install from appstore', function () {
        execSync('cloudron install --appstore-id io.hackmd.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, username));
    it('can create new note', newNote);
    it('can logout', logout);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, username));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
