#!/bin/bash

set -eu

# prepare data directory
mkdir -p /app/data/uploads /tmp/codimd /run/codimd

if [ ! -e /app/data/config.json ]; then
    echo "=> Creating initial template on first run"
	cp /app/pkg/config.json.template /app/data/config.json
fi

# generate and store an unique sessionSecret for this installation
CONFIG_JSON=/app/data/config.json
if [ $(jq .production.sessionSecret ${CONFIG_JSON}) == "null" ]; then
    echo "generating sessionSecret"
    sessionsecret=$(pwgen -1sc 32)
    jq ".production.sessionSecret = \"$sessionsecret\"" ${CONFIG_JSON} | sponge ${CONFIG_JSON}
fi

# these cannot be changed by user (https://github.com/hackmdio/hackmd/wiki/Environment-Variables)
export CMD_DOMAIN="${CLOUDRON_APP_DOMAIN}"
export CMD_PROTOCOL_USESSL=true
export CMD_DB_URL="${CLOUDRON_POSTGRESQL_URL}"
export CMD_LDAP_URL="${CLOUDRON_LDAP_URL}"
export CMD_LDAP_BINDDN="${CLOUDRON_LDAP_BIND_DN}"
export CMD_LDAP_BINDCREDENTIALS="${CLOUDRON_LDAP_BIND_PASSWORD}"
export CMD_LDAP_SEARCHBASE="${CLOUDRON_LDAP_USERS_BASE_DN}"
export CMD_LDAP_SEARCHFILTER="(|(username={{username}})(mail={{username}}))"
export CMD_LDAP_USERNAMEFIELD="username"
# legacy installations use uid based user ids. we have changes this to username for better portability
export CMD_LDAP_USERIDFIELD=$([[ -f /app/data/.ldap_uid ]] && echo "uid" || echo "username")
export CMD_PORT=3000
export CMD_TMP_PATH=/tmp/codimd
export CMD_USECDN=false

echo "=> Changing permissions"
chown -R cloudron:cloudron /app/data /tmp/codimd /run/codimd

# run
export NODE_ENV=production
exec /usr/local/bin/gosu cloudron:cloudron node app.js

