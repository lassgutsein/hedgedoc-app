FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

WORKDIR /app/code

ARG VERSION=1.9.7

# https://docs.hedgedoc.org/setup/manual-setup/
ARG NODEJS_VERSION=16.13.2

RUN mkdir -p /usr/local/node-${NODEJS_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODEJS_VERSION}

ENV PATH /usr/local/node-${NODEJS_VERSION}/bin:$PATH

RUN curl -L https://github.com/codimd/server/archive/${VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN yarn install --pure-lockfile && \
    yarn install --production=false --pure-lockfile && \
    npm run build

# Despite HMD_TMP_PATH, pdf generation seems to use /app/code/tmp
RUN rm -rf /app/code/public/uploads && ln -sf /app/data/uploads /app/code/public/uploads && \
    rm -rf /app/code/tmp && ln -sf /tmp/codimd /app/code/tmp

COPY config.json.template start.sh /app/pkg/
RUN ln -sfn /app/data/config.json /app/code/config.json

CMD ["/app/pkg/start.sh"]
